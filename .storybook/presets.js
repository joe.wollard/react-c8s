const path = require("path");

module.exports = [
  "@storybook/addon-docs/react/preset",
  {
    // Can go away once #65 is resolved
    // https://github.com/storybookjs/presets/issues/65
    name: "@storybook/preset-typescript",
    options: {
      include: [
        path.resolve(__dirname, "../.storybook"),
        path.resolve(__dirname, "../src"),
        path.resolve(__dirname, "../stories")
      ]
    }
  }
];
