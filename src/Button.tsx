import React, { ButtonHTMLAttributes } from "react";

export interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  /** Should this thing be blue or what??
   * @default false
   */
  blue?: boolean;
}

/**
 * This is a button, according to [w3.org](https://www.w3.org/TR/2019/NOTE-wai-aria-practices-1.1-20190814/examples/button/button.html).
 */
export function Button(props: ButtonProps) {
  return <button {...props} />;
}
