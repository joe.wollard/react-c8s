import React from "react";
import { action } from "@storybook/addon-actions";
import { Button } from "../src/Button";

export default {
  title: "Button",
  component: Button,
  parameters: { jest: ["Button"] }
};

export const basic = () => (
  <Button onClick={action("clicked")}>Hello Button</Button>
);

export const withEmoji = () => (
  <Button onClick={action("clicked")}>
    <span role="img" aria-label="so cool">
      😀 😎 👍 💯
    </span>
  </Button>
);
