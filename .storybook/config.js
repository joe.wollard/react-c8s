import { withA11y } from "@storybook/addon-a11y";
import { withTests } from "@storybook/addon-jest";
import { INITIAL_VIEWPORTS } from "@storybook/addon-viewport";
import { addDecorator, addParameters, configure } from "@storybook/react";
import results from "../.jest-test-results.json";

addParameters({
  viewport: {
    viewports: INITIAL_VIEWPORTS,
    defaultViewport: "desktop"
  }
});

addDecorator(withTests({ results }));
addDecorator(withA11y);

// automatically import all files ending in *.stories.tsx
configure(require.context("../stories", true, /\.stories\.tsx$/), module);
